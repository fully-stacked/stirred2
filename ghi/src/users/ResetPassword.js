import React from "react";
import styled from "styled-components";
import { AccountBox } from "../components/accountBox";

const LoginContainer = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

function ResetMain() {
  return (
    <LoginContainer>
      <AccountBox />
    </LoginContainer>
  );
}

export default ResetMain;
