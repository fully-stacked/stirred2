import React from "react";
import styled from "styled-components";
import { AccountBox } from "../components/accountBox";
import "../HeroSection.css";

const LoginContainer = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

function LoginMain() {
  return (
    <LoginContainer>
      <video
        className="video"
        src="Resources\loadingbeer.mp4"
        autoPlay
        loop
        muted
        type="video/mp4"
        style={{ filter: "blur(9px)" }}
      />
      <AccountBox />
    </LoginContainer>
  );
}

export default LoginMain;
