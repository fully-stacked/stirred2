import React, { useState } from "react";
import { Navigate, useParams } from "react-router-dom";
import { connect } from "react-redux";
import { verify } from "../actions/auth";
import { SubmitButton } from "../components/accountBox/common";

function Activate({ verify }) {
  const [verified, setVerified] = useState(false);

  const { uid } = useParams();
  const { token } = useParams();
  const verify_account = () => {
    verify(uid, token);
    setVerified(true);
  };

  if (verified) {
    return <Navigate to="/" />;
  }

  return (
    <div className="container mt-5">
      <div className="d-flex flex-column justify-content-center align-items-center mt-2">
        <h1>Verify your Account:</h1>
        <SubmitButton onClick={verify_account} type="button">
          Verify
        </SubmitButton>
      </div>
    </div>
  );
}

export default connect(null, { verify })(Activate);
