import React from "react";
import "./HeroSection.css";
import "./App.css";
import SearchHero from "./components/searchDB/searchHero";
function HeroSection() {
  return (
    <div className="hero-container">
      <video src="Resources\whisky.mp4" autoPlay loop muted type="video/mp4" />
      <img width="800" src="Resources/logo2.png" alt="" />
      <div className="hero-btns">
        <div className="row height d-flex justify-content-center align-items-center mt-2">
          <div className="col ">
            <SearchHero />
          </div>
        </div>
      </div>
    </div>
  );
}
export default HeroSection;
