import { Link } from "react-router-dom";
import React, { useEffect, useState } from "react";
import axios from "axios";
import Masonry from "react-masonry-css";
import "../Masonry.css";

function MyDrinks() {
  const [drinks, setDrinks] = useState(null);
  const [query, setQuery] = useState("");

  useEffect(() => {
    const getUserDrinks = async () => {
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `JWT ${localStorage.getItem("access")}`,
          Accept: "application/json",
        },
      };
      const res = await axios(
        `${process.env.REACT_APP_API_HOST}/auth/users/me/`,
        config
      );
      const currentUser = res.data.id;

      const drinkData = await axios(
        `${process.env.REACT_APP_API_HOST}/api/user/drinks/${currentUser}/`
      );

      setDrinks(drinkData.data.drinks);
    };
    getUserDrinks();
  }, []);

  const search = (data) => {
    const q = query.toLowerCase();
    return data.filter(
      (item) =>
        item.name.toLowerCase().includes(q) ||
        item.description.toLowerCase().includes(q) ||
        item.category.name.toLowerCase().includes(q)
    );
  };

  return (
    <div className="container mt-4">
      <div className="row height d-flex justify-content-center align-items-center">
        <img
          height="260"
          src="https://www.cointreau.com/themes/custom/wax_ui_subtheme/dist/img/content/c-cocktail-experience/reseller-bg.webp"
          alt=""
        />
        <h3 className="center">MY DRINKS</h3>
        <div className="search col-md-3 ">
          <input
            type="text"
            className="form-control "
            placeholder="Search your drink..."
            onChange={(e) => setQuery(e.target.value)}
          />
        </div>
      </div>
      <div
        className="row row-cols-3 mt-4 justify-content-center"
        style={{ marginBottom: "70px" }}
      >
        <Masonry
          breakpointCols={3}
          className="my-masonry-grid mt-3"
          columnClassName="my-masonry-grid_column"
        >
          {drinks &&
            search(drinks).map((drink) => (
              <div key={drink.id} className="col drinkcard">
                <div className="card mb-3 shadow">
                  <Link to={`/drinks/${drink.id}`}>
                    <img
                      src={drink.picture_url}
                      alt=""
                      className="card-img-top"
                    />
                  </Link>
                  <div className="card-body">
                    <h5 className="card-title">
                      <Link className="drink-name" to={`/drinks/${drink.id}`}>
                        {drink.name}
                      </Link>
                    </h5>
                    <p className="card-text">{drink.description}</p>
                  </div>
                </div>
              </div>
            ))}
        </Masonry>
      </div>
    </div>
  );
}

export default MyDrinks;
