import React from "react";
import axios from "axios";
import ReviewStar from "./ReviewStar";
import { FaCocktail } from "react-icons/fa";

class NewReviewPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: 0,
      comment: "",
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleRatingChange = this.handleRatingChange.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };

    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${localStorage.getItem("access")}`,
        Accept: "application/json",
      },
    };
    const res = await axios.get(
      `${process.env.REACT_APP_API_HOST}/auth/users/me/`,
      config
    );
    data.creator = res.data.id;

    const drink_url = window.location.href;
    const a = drink_url.split("/");
    let drink_id = 0;
    if (drink_url.includes("localhost")) {
      drink_id = Number(a[4]);
    } else {
      drink_id = Number(a[5]);
    }

    const reviewUrl = `${process.env.REACT_APP_API_HOST}/api/reviews/${drink_id}/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(reviewUrl, fetchConfig);
    if (response.ok) {
      await response.json();

      const cleared = {
        rating: 0,
        comment: "",
      };
      this.setState(cleared);
    }
  }

  handleFieldChange(event) {
    const value = event.target.value;
    this.setState({ [event.target.id]: value });
  }

  handleRatingChange(event) {
    let starrating = event.target.name;
    starrating = Number(starrating);
    this.setState({ rating: starrating });
  }

  render() {
    return (
      <div className="container mt-5">
        <p> What do you think about this drink?</p>
        <div>
          <ReviewStar
            curRating={this.state.rating}
            handleRatingChange={this.handleRatingChange}
          />
        </div>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group mt-4">
            <textarea
              className="form-control"
              type="text"
              id="comment"
              placeholder="Comment"
              name="comment"
              value={this.state.comment}
              onChange={this.handleFieldChange}
              required
            />
          </div>

          <div className="form-group mt-4">
            <button className="btn button-review" type="submit">
              <FaCocktail /> Rate this cocktail
            </button>
          </div>
        </form>
      </div>
    );
  }
}
export default NewReviewPage;
