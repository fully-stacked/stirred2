import { Link } from "react-router-dom";
import React, { useEffect, useState } from "react";
import Masonry from "react-masonry-css";
import "../Masonry.css";

function AllDrinks() {
  const [drinks, setDrinks] = useState(null);
  const [query, setQuery] = useState("");

  useEffect(() => {
    async function getDrinks() {
      const url = `${process.env.REACT_APP_API_HOST}/api/drinks/`;
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setDrinks(data.drinks);
      }
    }
    getDrinks();
  }, []);

  const search = (data) => {
    const q = query.toLowerCase();
    return data.filter(
      (item) =>
        item.name.toLowerCase().includes(q) ||
        item.description.toLowerCase().includes(q) ||
        item.category.name.toLowerCase().includes(q)
    );
  };

  return (
    <>
      <div className="container mt-4">
        <div className="row height d-flex justify-content-center align-items-center">
          <div className="col-md-8">
            <div className="search">
              <input
                type="text"
                className="form-control"
                placeholder="Search here"
                onChange={(e) => setQuery(e.target.value)}
              />
            </div>
          </div>
        </div>

        <div className="row row-cols-3 mt-4" style={{ marginBottom: "70px" }}>
          <Masonry
            breakpointCols={3}
            className="my-masonry-grid mt-3"
            columnClassName="my-masonry-grid_column"
          >
            {drinks &&
              search(drinks).map((drink) => (
                <div key={drink.id} className="col drinkcard">
                  <div className="card mb-3 shadow">
                    <Link to={`/drinks/${drink.id}`}>
                      <img
                        src={drink.picture_url}
                        className="card-img-top"
                        alt=""
                      />
                    </Link>
                    <div className="card-body">
                      <h5 className="card-title">
                        <Link className="drink-name" to={`/drinks/${drink.id}`}>
                          {drink.name}
                        </Link>
                      </h5>
                      <p className="card-text">{drink.description}</p>
                    </div>
                  </div>
                </div>
              ))}
          </Masonry>
        </div>
      </div>
    </>
  );
}

export default AllDrinks;
