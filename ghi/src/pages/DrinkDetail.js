import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import NewReviewPage from "./NewReview";
import { BsChatLeftQuote } from "react-icons/bs";
import FilledStar from "../stars/filled-star.svg";

function DrinkDetails(props) {
  const drinkId = useParams();

  const [drink, setDrink] = useState({});
  const [steps, setSteps] = useState([]);
  const [ingredientItem, setIngredients] = useState([]);
  const [userReviews, setReviews] = useState([]);

  useEffect(() => {
    async function getDrinkDetails() {
      const url = `${process.env.REACT_APP_API_HOST}/api/drinks/${drinkId.id}/`;
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setDrink(data);
      }
    }
    getDrinkDetails();
  }, [setDrink, drinkId]);

  useEffect(() => {
    async function getDrinkSteps() {
      const stepsUrl = `${process.env.REACT_APP_API_HOST}/api/drinks/${drinkId.id}/steps/`;
      const stepsResponse = await fetch(stepsUrl);
      if (stepsResponse.ok) {
        const stepsData = await stepsResponse.json();
        setSteps(stepsData.steps);
      }
    }
    getDrinkSteps();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setSteps]);

  useEffect(() => {
    async function getIngredients() {
      const ingUrl = `${process.env.REACT_APP_API_HOST}/api/drink/${drinkId.id}/ingredients/`;
      const ingResponse = await fetch(ingUrl);
      if (ingResponse.ok) {
        const ingData = await ingResponse.json();
        setIngredients(ingData.ingredients);
      }
    }
    getIngredients();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setIngredients]);

  useEffect(() => {
    async function getReviews() {
      const reviewUrl = `${process.env.REACT_APP_API_HOST}/api/reviews/${drinkId.id}/`;
      const reviewResponse = await fetch(reviewUrl);
      if (reviewResponse.ok) {
        const reviewData = await reviewResponse.json();
        setReviews(reviewData.reviews);
      }
    }
    getReviews();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setReviews]);

  return (
    <>
      <div className="container py-4 py-xl-5">
        <div className="row row-cols-1 row-cols-md-2">
          <div className="col">
            <img src={drink.picture_url} className="img-fluid" alt="" />
          </div>
          <div className="col d-flex flex-column justify-content-center p-4">
            <p className="drinkname">{drink.name}</p>
            <div className="text-center text-md-start d-flex flex-column align-items-center align-items-md-start mb-5">
              <div>
                <p>{drink.description}</p>
              </div>
            </div>
            <div className="text-center text-md-start d-flex flex-column align-items-center align-items-md-start mb-5">
              <div>
                <h3>Ingredients</h3>
                {ingredientItem.map((ingredient) => (
                  <div key={ingredient.id}>
                    <li>
                      {ingredient.amount} {ingredient.measurement}{" "}
                      {ingredient.name}
                    </li>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
        <div className="text-center text-md-start d-flex flex-column align-items-center align-items-md-start container py-4 py-xl-5 width">
          <div className="container py-4 py-xl-5">
            <h3>Instructions</h3>
            {steps.map((step) => (
              <div key={step.id}>
                <li>{step.directions}</li>
              </div>
            ))}
          </div>
        </div>
      </div>

      <div className="container py-4 py-xl-5 width">
        <NewReviewPage />
        <br />
        <br />
        <div>
          <div className="review-items row row-cols-1 row-cols-md-2">
            {userReviews.map((user) => (
              <div key={user.id} className="card mb-3 reviewcards">
                <div className="card-body">
                  <p>
                    <BsChatLeftQuote />
                  </p>
                  <h6 className="card-title name">
                    {user.creator.first_name}{" "}
                  </h6>
                  {(() => {
                    let reviews = [];
                    for (let i = 1; i <= user.rating; i++) {
                      reviews.push(
                        <p className="starstar" key={i}>
                          {" "}
                          <img src={FilledStar} className="rstar" alt="" />
                        </p>
                      );
                    }
                    return reviews;
                  })()}
                  <p className="card-text">{user.comment}</p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  );
}
export default DrinkDetails;
