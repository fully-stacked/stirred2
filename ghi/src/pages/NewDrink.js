import React from "react";
import { NewDrinkBox } from "../components/newDrinkBox";
import styled from "styled-components";
import "../HeroSection.css";

const NewDrinkContainer = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

function NewDrink() {
  return (
    <NewDrinkContainer>
      <video
        className="video"
        src="Resources\newdrink.mp4"
        autoPlay
        loop
        muted
        type="video/mp4"
        style={{ filter: "blur(9px)" }}
      />
      <NewDrinkBox />
    </NewDrinkContainer>
  );
}

export default NewDrink;
