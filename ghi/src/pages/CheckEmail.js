import React from "react";
import { SubmitButton } from "../components/accountBox/common";
import { Link } from "react-router-dom";

function CheckEmail() {
  return (
    <div className="container mt-5">
      <div className="d-flex flex-column justify-content-center align-items-center mt-2">
        <h1>Please check email for verification link</h1>
        <Link to="/">
          <SubmitButton
            style={{
              padding: "10px 20%",
              justifyContent: "center",
              alignText: "center",
            }}
            type="button"
          >
            Return Home
          </SubmitButton>
        </Link>
      </div>
    </div>
  );
}

export default CheckEmail;
