import EmptyStar from "../stars/empty-star.svg";
import FilledStar from "../stars/filled-star.svg";

const ReviewStar = ({ curRating, handleRatingChange }) => {
  const stars = [1, 2, 3, 4, 5];
  return (
    <div>
      {stars.map((star) => {
        return (
          <img
            name={star}
            key={star}
            className="star"
            onClick={(event) => handleRatingChange(event)}
            src={star <= curRating ? FilledStar : EmptyStar}
            alt={star <= curRating ? "filled star" : "empty star"}
          />
        );
      })}
    </div>
  );
};
export default ReviewStar;
