import React from "react";
import "./Button.css";
import { logout } from "./actions/auth";
import { connect } from "react-redux";

const STYLES = ["btn--primary", "btn-outline"];

const SIZES = ["btn--medium", "button--large"];

function LogoutButton({ children, type, logout, buttonStyle, buttonSize }) {
  const checkButtonStyle = STYLES.includes(buttonStyle)
    ? buttonStyle
    : STYLES[0];

  const checkButtonSize = SIZES.includes(buttonSize) ? buttonSize : SIZES[0];

  return (
    <div className="btn-mobile">
      <button
        className={`btn btn-mobile ${checkButtonStyle} ${checkButtonSize}`}
        onClick={logout}
        type={type}
      >
        {children}
      </button>
    </div>
  );
}
const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { logout })(LogoutButton);
