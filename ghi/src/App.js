import { Route, BrowserRouter, Routes } from "react-router-dom";
import AllDrinksPage from "./pages/AllDrinks";
import NewReviewPage from "./pages/NewReview";
import LoginMain from "./users/Login";
import Activate from "./users/Activate";
import ResetPasswordConfirm from "./users/ResetPasswordConfirm";
import Layout from "./layout/Layout";
import { Provider } from "react-redux";
import store from "./store";
import "./App.css";
import MainPage from "./MainPage";
import DrinkDetail from "./pages/DrinkDetail";
import SignupMain from "./users/Signup";
import NewDrink from "./pages/NewDrink";
import ResetMain from "./users/ResetPassword";
import MyDrinks from "./pages/MyDrinks";
import DrinkDetails from "./components/searchDB/showDrinks";
import CheckEmail from "./pages/CheckEmail";
import Results from "./components/searchDB/results";

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");
  return (
    <Provider store={store}>
      <BrowserRouter basename={basename}>
        <Layout>
          <Routes>
            <Route path="/" exact element={<MainPage />} />
            <Route path="/drinks" element={<AllDrinksPage />} />
            <Route path="/drinks/:id" element={<DrinkDetail />} />
            <Route path="/new-drink" element={<NewDrink />} />
            <Route path="/new-review/:id" element={<NewReviewPage />} />
            <Route path="/login" element={<LoginMain />} />
            <Route path="/signup" element={<SignupMain />} />
            <Route path="/reset-password" element={<ResetMain />} />
            <Route
              path="/password/reset/confirm/:uid/:token"
              element={<ResetPasswordConfirm />}
            />
            <Route path="/mydrinks" element={<MyDrinks />} />
            <Route path="/activate/:uid/:token" element={<Activate />} />
            <Route path="/db/:id" element={<DrinkDetails />} />
            <Route path="/checkemail" element={<CheckEmail />} />
            <Route path="/results" element={<Results />} />
          </Routes>
        </Layout>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
