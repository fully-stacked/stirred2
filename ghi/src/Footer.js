import React from "react";
import "./Footer.css";

function Footer() {
  return (
    <footer className="main-footer">
      <div className="container">
        <div className="row">
          <div className="col">
            <h4>Meet the Dev Team !</h4>
            <ul className="list-unstyled">
              <li>
                <a href="https://www.linkedin.com/in/jasonhtnguyen/">Jason</a>
              </li>
              <li>
                <a href="https://www.linkedin.com/in/johneger17">John</a>
              </li>

              <li>
                <a href="https://www.linkedin.com/in/jonathanchen888/">
                  Jonathan
                </a>
              </li>
              <li>
                <a href="https://www.linkedin.com/in/malikomarswe/">Malik</a>
              </li>
              <li>
                <a href="https://www.linkedin.com/in/helenadou11/">Menger</a>
              </li>
            </ul>
          </div>
          <div className="row">
            <div className="col-sm">
              &copy;{new Date().getFullYear()} Stirred Inc | All Rights Reserved
              | Terms of Service | Privacy
            </div>
          </div>
        </div>
        <h1>Stirred</h1>
      </div>
    </footer>
  );
}

export default Footer;
