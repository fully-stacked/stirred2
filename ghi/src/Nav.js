import React, { useState, useEffect, Fragment } from "react";
import { Button } from "./Button";
import { Link } from "react-router-dom";
import "./Nav.css";
import { connect } from "react-redux";
import LogoutButton from "./LogoutButton";

function Navbar({ isAuthenticated }) {
  const [click, setClick] = useState(false);
  const [button, setButton] = useState(true);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const guestButton = () => (
    <Fragment>
      {button && <Button buttonStyle="btn--outline">SIGN UP</Button>}
    </Fragment>
  );

  const authButton = () => (
    <Fragment>
      {<LogoutButton buttonStyle="btn--outline">LOGOUT</LogoutButton>}
    </Fragment>
  );
  const guestLinks = () => <Fragment></Fragment>;

  const authLinks = () => (
    <Fragment>
      <li className="nav-item">
        <Link to="/new-drink" className="nav-links" onClick={closeMobileMenu}>
          New Drink
        </Link>
      </li>
      <li className="nav-item">
        <Link to="/mydrinks" className="nav-links" onClick={closeMobileMenu}>
          My Drinks
        </Link>
      </li>
    </Fragment>
  );

  const showButton = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
    } else {
      setButton(true);
    }
  };

  useEffect(() => {
    showButton();
  }, []);

  window.addEventListener("resize", showButton);

  return (
    <>
      <nav className="Navbar">
        <div className="navbar-container">
          <Link to="/" className="navbar-logo" onClick={closeMobileMenu}>
            Stirred
          </Link>
          <div className="menu-icon" onClick={handleClick}>
            <i className={click ? "fas fa-times" : "fas fa-bars"} />
          </div>
          <ul className={click ? "nav-menu active" : "nav-menu"}>
            <li className="nav-item">
              <Link to="/" className="nav-links" onClick={closeMobileMenu}>
                Home
              </Link>
            </li>
            <li className="nav-item">
              <Link
                to="/drinks"
                className="nav-links"
                onClick={closeMobileMenu}
              >
                Creations
              </Link>
            </li>

            {isAuthenticated ? authLinks() : guestLinks()}

            <li>
              <Link
                to="/signup"
                className="nav-links-mobile"
                onClick={closeMobileMenu}
              >
                Sign Up
              </Link>
            </li>
          </ul>
          {isAuthenticated ? authButton() : guestButton()}
        </div>
      </nav>
    </>
  );
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, {})(Navbar);
