import React from "react";
import Cards from "./Cards";
import "./AllCards.css";

function AllCards() {
  return (
    <div className="cards">
      <h1>Check out these AWESOME Creations!</h1>
      <div className="cards__container">
        <div className="cards__wrapper">
          <ul className="cards__items">
            <Cards
              src="Resources/expresso.jpg"
              text="TRY OUT THIS AMAZING ESPRESSO MARTINI"
              label="Martini"
              path="/drinks/8"
            />
            <Cards
              src="Resources/mint_mojito.jpg"
              text="SIT BACK AND ENJOY A COOLING MINT MOJITO"
              label="mint_mojito"
              path="/drinks/9"
            />
            <Cards
              src="Resources/tequila_sunrise.jpg"
              text="COOL DOWN WITH A REFRESHING TEQUILA SUNRISE"
              label="tequila_sunrise"
              path="/drinks/3"
            />
          </ul>
        </div>
      </div>
    </div>
  );
}

export default AllCards;
