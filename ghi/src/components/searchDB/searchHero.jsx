import React, { useState } from "react";
import axios from "axios";
import "./searchDB.css";
import { Marginer } from "../../marginer/marginer";
import { SubmitButton, FormContainer, Input } from "../accountBox/common";
import { useNavigate } from "react-router-dom";

function shuffle(drinkArr) {
  let curIdx = drinkArr.length;
  while (curIdx !== 0) {
    let randomIndx = Math.floor(Math.random() * curIdx);
    curIdx--;
    [drinkArr[curIdx], drinkArr[randomIndx]] = [
      drinkArr[randomIndx],
      drinkArr[curIdx],
    ];
  }

  return drinkArr;
}

function SearchHero() {
  const navigate = useNavigate();
  const [search, setSearch] = useState([]);
  const [data, setData] = useState(null);
  const [filter, setFilter] = useState("ingredients");

  const onChange = (e) => {
    const searchData = { ...search, [e.target.name]: e.target.value };
    setSearch({ search: searchData });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    const url = filter === "ingredients" ? "filter.php?i" : "search.php?s";
    if (search.length !== 0) {
      const res = await axios(
        `https://www.thecocktaildb.com/api/json/v1/1/${url}=${search.search.textInput}`
      );
      const drinksFetched = shuffle(res.data.drinks);
      if (drinksFetched.length >= 20) {
        const limitedDrinks = [];
        let i = 0;
        for (const drink of drinksFetched) {
          limitedDrinks.push(drink);
          i++;
          if (i === 21) {
            break;
          }
        }
        setData({ data: limitedDrinks });
      } else {
        setData({ data: drinksFetched });
      }
    }
  };

  if (data !== null) {
    navigate("/results", { state: { ...data } });
  }

  const changeFilter = (e) => {
    const filterBy = e.target.value;
    setFilter({ filter: filterBy });
  };
  return (
    <div className="container">
      <section>
        <FormContainer onSubmit={(e) => onSubmit(e)}>
          <div>
            <div onChange={(e) => changeFilter(e)}>
              <input
                type="radio"
                name="searchFilter"
                value="ingredients"
                id="ing"
              />{" "}
              <label htmlFor="ing">Ingredient</label>{" "}
              <input type="radio" name="searchFilter" value="name" id="name" />
              <label htmlFor="name">Cocktail name</label>
            </div>
          </div>
          <div>
            <Marginer direction="vertical" margin="1em" />
            <div>
              <Input
                className="input"
                type="text"
                name="textInput"
                placeholder="eg. Vodka or Old fashioned"
                onChange={(e) => onChange(e)}
              />
            </div>
            <Marginer direction="vertical" margin="1em" />
            <div
              style={{
                width: "100%",
                justifyContent: "center",
              }}
            >
              <SubmitButton
                style={{
                  padding: "10px 20%",
                  justifyContent: "center",
                  alignText: "center",
                }}
                type="submit"
              >
                Search
              </SubmitButton>
            </div>
          </div>
        </FormContainer>
      </section>
    </div>
  );
}

export default SearchHero;
