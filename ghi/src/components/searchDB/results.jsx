import React from "react";
import { useLocation } from "react-router-dom";
import DrinkDetailList from "./drinkDetailList";

function Results() {
  const data = useLocation();
  return <div>{<DrinkDetailList {...data.state} />}</div>;
}

export default Results;
