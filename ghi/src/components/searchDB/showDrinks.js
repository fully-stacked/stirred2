import React, { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";

export default function DrinkDetails() {
  const drinkId = useParams();
  const [drank, setDrink] = useState([]);
  useEffect(() => {
    async function getDrinkDetails() {
      const res = await axios(
        `https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${drinkId.id}`
      );
      const data = res.data.drinks[0];
      const drinks = Object.keys(data)
        .filter((key) => key.match(/ingredient/i))
        .filter((key) => !!data[key] || data[key] === "")
        .map((key) => data[key].trim());

      const measures = Object.keys(data)
        .filter((key) => key.match(/measure/i))
        .filter((key) => !!data[key] || data[key] === " ")
        .map((key) => data[key].trim());

      const ingredients = drinks.map((drink, index) => {
        return { drink: drink, measure: measures[index] };
      });

      const cocktail = {
        image: data.strDrinkThumb,
        name: data.strDrink,
        instructions: data.strInstructions,
        glass: data.strGlass,
        alcoholic: data.strAlcoholic,
        category: data.strCategory,
        id: data.idDrink,
        ingredients,
      };
      setDrink([cocktail]);
    }
    getDrinkDetails();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setDrink]);

  if (!drank) return null;
  return (
    <div className="container py-4 py-xl-5">
      <div className="row row-cols-1 row-cols-md-2">
        <div className="col">
          {drank.map((image) => {
            return (
              <div key={image.image}>
                <img src={image.image} alt="" className="img-fluid" />
              </div>
            );
          })}
        </div>
        <div className="col d-flex flex-column justify-content-center p-4">
          {drank.map((name) => {
            return (
              <div key={name.name}>
                <p style={{ color: "#df6c49" }} className="drinkname">
                  {name.name}
                </p>
              </div>
            );
          })}
          <div className="text-center text-md-start d-flex flex-column align-items-center align-items-md-start mb-5">
            <div>
              <div>
                <h3
                  style={{
                    color: "#df6c49",
                    textDecorationLine: "underline",
                    textUnderlinePosition: "auto",
                  }}
                >
                  Ingredients
                </h3>
              </div>
              <ul className="list-group list-group-flush">
                {drank.map((ing) => {
                  return ing.ingredients.map((ingredient) => {
                    return (
                      <li key={ingredient.drink} className="list-group-item">
                        <strong>{ingredient.drink}</strong> -{" "}
                        <strong>{ingredient.measure}</strong>
                      </li>
                    );
                  });
                })}
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div className="text-center text-md-start d-flex flex-column align-items-center align-items-md-start container py-4 py-xl-5 width">
        <div className="container py-4 py-xl-5">
          <h3
            style={{
              color: "#df6c49",
              textDecorationLine: "underline",
              textUnderlinePosition: "auto",
            }}
          >
            Instructions
          </h3>
          {drank.map((steps) => {
            return (
              <div key={steps.instructions}>
                <p>{steps.instructions}</p>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
