import React from "react";

const DrinkCard = (drinks) => {
  return (
    <div className="card mt-4 shadow">
      <div className="card-image">
        <figure>
          {
            <img
              className="card-img-top"
              src={drinks.strDrinkThumb}
              alt={drinks.strDrink}
            />
          }
        </figure>
      </div>
      <div className="card-content">
        <h6
          style={{
            color: "#df6c49",
            textDecorationLine: "underline",
            textUnderlinePosition: "auto",
            textAlign: "center",
          }}
          className="subtitle is-5"
        >
          {drinks.strDrink}
        </h6>
      </div>
    </div>
  );
};

export default DrinkCard;
