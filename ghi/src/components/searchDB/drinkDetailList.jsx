import React from "react";
import { Link } from "react-router-dom";
import DrinkCard from "./dbCard";

export default function DrinkDetailList(data) {
  if (!data.data) return null;
  return (
    <div className="container mt-4">
      <div className="row row-cols-3 mt-4">
        {data.data.map((drinks) => (
          <div key={drinks.idDrink}>
            <Link to={`/db/${drinks.idDrink}`}>
              <DrinkCard {...drinks} />
            </Link>
          </div>
        ))}
      </div>
    </div>
  );
}
