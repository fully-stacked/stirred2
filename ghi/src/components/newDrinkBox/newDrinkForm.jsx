import React, { useState, useContext, useEffect } from "react";
import { Marginer } from "../../marginer/marginer";
import { AccountContext } from "../accountBox/accountsContext";
import axios from "axios";
import {
  BoxContainerNewCommon,
  FormContainerNew,
  InputNew,
  SubmitButtonNew,
  TextAreaNew,
  SelectNew,
} from "./common";

function NewDrinkForm() {
  const { switchToIngredients } = useContext(AccountContext);
  const [user, setUser] = useState({ user: null });
  const [categories, setCategories] = useState([]);
  const [formData, setFormData] = useState({
    name: "",
    alcoholic: "",
    description: "",
    picture_url: "",
    category: "",
    creator: "",
  });

  useEffect(() => {
    const getUser = async () => {
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `JWT ${localStorage.getItem("access")}`,
          Accept: "application/json",
        },
      };
      const res = await axios(
        `${process.env.REACT_APP_API_HOST}/auth/users/me/`,
        config
      );
      const currentUser = res.data.id;
      setUser({ user: currentUser });
    };

    const getCategories = async () => {
      const res = await axios(
        `${process.env.REACT_APP_API_HOST}/api/categories/`
      );
      setCategories(res.data.categories);
    };

    getCategories();
    getUser();
  }, []);

  const validate = (name, alcoholic, description, picture_url, category) => {
    if (
      name === "" ||
      alcoholic === "" ||
      description === "" ||
      picture_url === "" ||
      category === ""
    ) {
      return false;
    } else {
      return true;
    }
  };

  const { name, alcoholic, description, picture_url, category } = formData;
  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async (e) => {
    e.preventDefault();
    formData.creator = JSON.stringify(user.user);
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    const body = JSON.stringify({ ...formData });
    await axios.post(
      `${process.env.REACT_APP_API_HOST}/api/drinks/`,
      body,
      config
    );
    const cleared = {
      name: "",
      alcoholic: "",
      description: "",
      picture_url: "",
      category: "",
      creator: "",
    };
    setFormData(cleared);
  };
  return (
    <BoxContainerNewCommon>
      <FormContainerNew onSubmit={(e) => onSubmit(e)}>
        <InputNew
          type="text"
          placeholder="Drink name"
          name="name"
          value={name}
          onChange={(e) => onChange(e)}
          required
        />
        <SelectNew
          name="alcoholic"
          value={alcoholic}
          onChange={(e) => onChange(e)}
          required
        >
          <option value="">Is your drink alcoholic?</option>
          <option value="True">Yes</option>
          <option value="False">No</option>
        </SelectNew>
        <TextAreaNew
          type="text"
          placeholder="Description"
          name="description"
          value={description}
          onChange={(e) => onChange(e)}
          required
        />
        <InputNew
          type="text"
          placeholder="Picture url"
          name="picture_url"
          value={picture_url}
          onChange={(e) => onChange(e)}
          required
        />
        <SelectNew
          placeholder="Category"
          name="category"
          value={category}
          onChange={(e) => onChange(e)}
          required
        >
          <option value="">Select a category</option>
          {categories.map((category) => {
            return (
              <option key={category.id} value={category.id}>
                {category.name}
              </option>
            );
          })}
        </SelectNew>
        <Marginer direction="vertical" margin="1.6em" />
        <SubmitButtonNew
          onClick={
            validate(
              (formData.name,
              formData.alcoholic,
              formData.description,
              formData.picture_url,
              formData.category)
            )
              ? switchToIngredients
              : undefined
          }
          type="submit"
        >
          Create
        </SubmitButtonNew>
        <Marginer direction="vertical" margin="1.6em" />
      </FormContainerNew>
    </BoxContainerNewCommon>
  );
}

export default NewDrinkForm;
