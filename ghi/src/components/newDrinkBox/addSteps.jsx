import React, { useState, useContext, useEffect } from "react";
import { Marginer } from "../../marginer/marginer";
import { AccountContext } from "../accountBox/accountsContext";
import axios from "axios";
import {
  BoldLink,
  BoxContainer,
  FormContainer,
  Input,
  SubmitButton,
} from "../accountBox/common";

function AddSteps() {
  const { switchToIngredients } = useContext(AccountContext);
  const [info, setInfo] = useState({ user: null, drink: null });
  const [formData, setFormData] = useState({
    order: "",
    directions: "",
    drink: 0,
  });

  useEffect(() => {
    const getUser = async () => {
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `JWT ${localStorage.getItem("access")}`,
          Accept: "application/json",
        },
      };
      const res = await axios(
        `${process.env.REACT_APP_API_HOST}/auth/users/me/`,
        config
      );
      const currentUser = res.data.id;

      const drinkData = await axios(
        `${process.env.REACT_APP_API_HOST}/api/user/drinks/${currentUser}/`
      );
      const currentDrink =
        drinkData.data.drinks[drinkData.data.drinks.length - 1];

      setInfo({ user: currentUser, drink: currentDrink });
    };
    getUser();
  }, []);

  const { order, directions, drink } = formData;
  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    formData.drink = info.drink.id;
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    const body = JSON.stringify({ ...formData });
    await axios.post(
      `${process.env.REACT_APP_API_HOST}/api/drinks/${formData.drink}/steps/`,
      body,
      config
    );
    const cleared = {
      order: "",
      directions: "",
      drink: 0,
    };
    setFormData(cleared);
  };

  return (
    <BoxContainer>
      <FormContainer onSubmit={(e) => onSubmit(e)}>
        <Input
          type="number"
          step="1"
          min="1"
          placeholder="Which step is this?"
          name="order"
          value={order}
          onChange={(e) => onChange(e)}
          required
        />
        <Input
          type="text"
          placeholder="Directions"
          name="directions"
          value={directions}
          onChange={(e) => onChange(e)}
          required
        />
        <Input
          hidden
          type="number"
          name="drink"
          value={drink}
          onChange={(e) => onChange(e)}
          required
        />
        <Marginer direction="vertical" margin="1.6em" />
        <SubmitButton type="submit">Add</SubmitButton>
      </FormContainer>
      <Marginer direction="vertical" margin="1em" />
      <BoldLink
        onClick={switchToIngredients}
        style={{ color: "#df6c49", fontSize: "15px" }}
      >
        ← Click here to add another ingredient!
      </BoldLink>
    </BoxContainer>
  );
}

export default AddSteps;
