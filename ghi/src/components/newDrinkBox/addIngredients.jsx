import React, { useState, useContext, useEffect } from "react";
import { Marginer } from "../../marginer/marginer";
import { AccountContext } from "../accountBox/accountsContext";
import axios from "axios";
import {
  BoldLink,
  BoxContainer,
  FormContainer,
  Input,
  SubmitButton,
} from "../accountBox/common";

function AddIngredients() {
  const { switchToSteps } = useContext(AccountContext);
  const [info, setInfo] = useState({ user: null, drink: null });
  const [formData, setFormData] = useState({
    name: "",
    amount: "",
    measurement: "",
    drink: 0,
  });

  useEffect(() => {
    const getUser = async () => {
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `JWT ${localStorage.getItem("access")}`,
          Accept: "application/json",
        },
      };
      const res = await axios(
        `${process.env.REACT_APP_API_HOST}/auth/users/me/`,
        config
      );
      const currentUser = res.data.id;

      const drinkData = await axios(
        `${process.env.REACT_APP_API_HOST}/api/user/drinks/${currentUser}/`
      );
      const currentDrink =
        drinkData.data.drinks[drinkData.data.drinks.length - 1];

      setInfo({ user: currentUser, drink: currentDrink });
    };
    getUser();
  }, []);

  const { name, amount, measurement, drink } = formData;
  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    formData.drink = info.drink.id;
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    const body = JSON.stringify({ ...formData });
    await axios.post(
      `${process.env.REACT_APP_API_HOST}/api/drink/${formData.drink}/ingredients/`,
      body,
      config
    );
    const cleared = {
      name: "",
      amount: "",
      measurement: "",
      drink: 0,
    };
    setFormData(cleared);
  };

  return (
    <BoxContainer>
      <FormContainer onSubmit={(e) => onSubmit(e)}>
        <Input
          type="text"
          placeholder="Name (Vodka...Lemon zest)"
          name="name"
          value={name}
          onChange={(e) => onChange(e)}
          required
        />

        <Input
          type="number"
          step="0.1"
          min="0"
          placeholder="Amount (1...2.5)"
          name="amount"
          value={amount}
          onChange={(e) => onChange(e)}
          required
        />
        <Input
          type="text"
          placeholder="Measurement (Ounces...mL)"
          name="measurement"
          value={measurement}
          onChange={(e) => onChange(e)}
        />
        <Input
          hidden
          type="number"
          name="drink"
          value={drink}
          onChange={(e) => onChange(e)}
          required
        />
        <Marginer direction="vertical" margin="1.6em" />
        <SubmitButton type="submit">Add</SubmitButton>
      </FormContainer>
      <Marginer direction="vertical" margin="1.6em" />
      <BoldLink
        onClick={switchToSteps}
        style={{ color: "#df6c49", fontSize: "15px" }}
      >
        Don't forget to add directions to your drink! →
      </BoldLink>
      <Marginer direction="vertical" margin="1.6em" />
    </BoxContainer>
  );
}

export default AddIngredients;
