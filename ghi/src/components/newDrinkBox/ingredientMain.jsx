import React from "react";
import styled from "styled-components";
import { NewDrinkBox } from ".";

const NewDrinkContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

function IngredientMain() {
  return (
    <NewDrinkContainer>
      <NewDrinkBox />
    </NewDrinkContainer>
  );
}

export default IngredientMain;
