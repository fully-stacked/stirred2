import React, { useState } from "react";
import styled from "styled-components";
import { motion } from "framer-motion";
import { AccountContext } from "../accountBox/accountsContext";
import NewDrinkForm from "./newDrinkForm";
import AddIngredients from "./addIngredients";
import AddSteps from "./addSteps";

const BoxContainerNew = styled.div`
  width: 420px;
  min-height: 550px;
  display: flex;
  flex-direction: column;
  border-radius: 19px;
  background-color: #fff;
  box-shadow: rgba(50, 50, 93, 0.25) 0px 6px 12px -2px,
    rgba(0, 0, 0, 0.3) 0px 3px 7px -3px;
  position: relative;
  overflow: hidden;
`;

const TopContainerNew = styled.div`
  width: 100%;
  height: 250px;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  padding: 0 1.8em;
  padding-bottom: 5em;
`;

const BackDropNew = styled(motion.div)`
  width: 160%;
  height: 550px;
  position: absolute;
  display: flex;
  flex-direction: column;
  border-radius: 50%;
  transform: rotate(120deg);
  top: -350px;
  left: -150px;
  background: linear-gradient(54deg, #df6c49 22%, rgba(254, 155, 56, 1) 50%);
`;

const HeaderContainerNew = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const HeaderTextNew = styled.h2`
  font-size: 30px;
  font-weight: 600;
  line-height: 1.24;
  color: #fff;
  z-index: 10;
  margin: 0;
`;

const InnerContainerNew = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  padding: 0 1.8em;
`;

const backdropVariantsNew = {
  expanded: {
    width: "233%",
    height: "1150px",
    borderRadius: "30%",
    transform: "rotate(80deg)",
  },
  collapsed: {
    width: "160%",
    height: "550px",
    borderRadius: "60%",
    transform: "rotate(-60deg)",
  },
};

const expandingTransitionNew = {
  type: "spring",
  duration: 3.0,
  stiffness: 30,
};

export function NewDrinkBox() {
  const [isExpanded, setExpanded] = useState(false);
  const [form, setForm] = useState("newDrink");

  const playExpandingAnimation = () => {
    setExpanded(true);
    setTimeout(() => {
      setExpanded(false);
    }, expandingTransitionNew.duration * 1000 - 1800);
  };

  const switchToIngredients = () => {
    playExpandingAnimation();
    setTimeout(() => {
      setForm("ingredients");
    }, 400);
  };

  const switchToSteps = () => {
    playExpandingAnimation();
    setTimeout(() => {
      setForm("steps");
    }, 400);
  };

  const contextValue = { switchToIngredients, switchToSteps };

  return (
    <AccountContext.Provider value={contextValue}>
      <BoxContainerNew>
        <TopContainerNew>
          <BackDropNew
            initial={false}
            animate={isExpanded ? "expanded" : "collapsed"}
            variants={backdropVariantsNew}
            transition={expandingTransitionNew}
          />
          {form === "newDrink" && (
            <HeaderContainerNew>
              <HeaderTextNew>Create your own</HeaderTextNew>
              <HeaderTextNew>cocktail!</HeaderTextNew>
            </HeaderContainerNew>
          )}
          {form === "ingredients" && (
            <HeaderContainerNew>
              <HeaderTextNew>Add ingredients to your</HeaderTextNew>
              <HeaderTextNew>cocktail!</HeaderTextNew>
            </HeaderContainerNew>
          )}
          {form === "steps" && (
            <HeaderContainerNew>
              <HeaderTextNew>Add directions to your</HeaderTextNew>
              <HeaderTextNew>cocktail!</HeaderTextNew>
            </HeaderContainerNew>
          )}
        </TopContainerNew>
        <InnerContainerNew>
          {form === "newDrink" && <NewDrinkForm />}
          {form === "ingredients" && <AddIngredients />}
          {form === "steps" && <AddSteps />}
        </InnerContainerNew>
      </BoxContainerNew>
    </AccountContext.Provider>
  );
}
