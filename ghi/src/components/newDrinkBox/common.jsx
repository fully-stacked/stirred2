import styled from "styled-components";

export const BoxContainerNewCommon = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 10px;
`;

export const FormContainerNew = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  box-shadow: 0px, 0px, 2.5px rgba(15, 15, 15, 0.19);
`;

export const MutedLinkNew = styled.p`
  font-size: 12px;
  color: rgba(200, 200, 200, 1);
  font-weight: 500;
  text-decoration: none;
`;

export const BoldLinkNew = styled.a`
  font-size: 20px;
  align-items: center;
  justify-content: center
  color: #df6c49;
  font-weight: 500;
  text-decoration: none;
  margin: 0 4px;
`;

export const InputNew = styled.input`
  width: 100%;
  height: 42px;
  outline: none;
  border: 1px solid rgba(200, 200, 200, 0.3);
  padding: 0px 10px;
  border-bottom: 1.4px solid transparent;
  transition: all 200ms ease-in-out;
  font-size: 13px

  &::placeholder {
    color: rgba(200, 200, 200, 1);
  }

  &:not(:last-of-type) {
    border-bottom: 1.5px solid #df6c49;
  }

  &:focus {
    outline: none;
    border-bottom: 2px solid #df6c49;
  }
`;

export const SelectNew = styled.select`
  width: 100%;
  height: 42px;
  outline: none;
  border: 1px solid rgba(200, 200, 200, 0.3);
  padding: 0px 10px;
  border-bottom: 1.4px solid transparent;
  transition: all 200ms ease-in-out;
  font-size: 13px

  &::placeholder {
    color: rgba(200, 200, 200, 1);
  }

  &:not(:last-of-type) {
    border-bottom: 1.5px solid #df6c49;
  }

  &:focus {
    outline: none;
    border-bottom: 2px solid #df6c49;
  }
`;

export const TextAreaNew = styled.textarea`
  width: 100%;
  height: 42px;
  outline: none;
  border: 1px solid rgba(200, 200, 200, 0.3);
  padding: 0px 10px;
  border-bottom: 1.4px solid transparent;
  transition: all 200ms ease-in-out;
  font-size: 13px

  &::placeholder {
    color: rgba(200, 200, 200, 1);
  }

  &:not(:last-of-type) {
    border-bottom: 1.5px solid #df6c49;
  }

  &:focus {
    outline: none;
    border-bottom: 2px solid #df6c49;
  }
`;

export const SubmitButtonNew = styled.button`
  width: 100%;
  padding: 14px 40%;
  color: #fff;
  font-size: 15px;
  font-weight: 600;
  border: none;
  border-radius: 100px 100px 100px 100px;
  cursor: pointer;
  transition: all, 240ms, ease-in-out;
  background: linear-gradient(54deg, #df6c49 22%, rgba(254, 155, 56, 1) 50%);

  &:hover {
    filter: brightness(1.03);
  }
`;
