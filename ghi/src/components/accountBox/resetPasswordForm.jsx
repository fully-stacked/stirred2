import React, { useState, useContext } from "react";
import { Navigate } from "react-router-dom";
import { connect } from "react-redux";
import { reset_password } from "../../actions/auth";
import { Marginer } from "../../marginer/marginer";
import { AccountContext } from "./accountsContext";
import {
  BoldLink,
  BoxContainer,
  FormContainer,
  Input,
  MutedLink,
  SubmitButton,
} from "./common";

function ResetPassword({ reset_password }) {
  const { switchToLogin } = useContext(AccountContext);

  const [requestSent, setRequestSent] = useState(false);
  const [formData, setFormData] = useState({
    email: "",
  });

  const { email } = formData;
  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });
  const onSubmit = (e) => {
    e.preventDefault();

    reset_password(email);
    setRequestSent(true);
  };

  if (requestSent) {
    return <Navigate to="/" />;
  }

  return (
    <BoxContainer className="container mt-5">
      <FormContainer onSubmit={(e) => onSubmit(e)}>
        <div className="form-group mt-2">
          <Input
            type="email"
            placeholder="Email"
            name="email"
            value={email}
            onChange={(e) => onChange(e)}
            required
          />
        </div>
        <Marginer direction="vertical" margin="1.6em" />
        <SubmitButton type="submit">Reset</SubmitButton>
      </FormContainer>
      <Marginer direction="vertical" margin="1em" />
      <MutedLink>
        Already have an account?{" "}
        <BoldLink onClick={switchToLogin} style={{ color: "#df6c49" }}>
          Login
        </BoldLink>
      </MutedLink>
    </BoxContainer>
  );
}

export default connect(null, { reset_password })(ResetPassword);
