import React, { useState, useContext } from "react";
import { Navigate } from "react-router-dom";
import { connect } from "react-redux";
import { signup } from "../../actions/auth";
import {
  BoldLink,
  BoxContainer,
  FormContainer,
  Input,
  MutedLink,
  SubmitButton,
} from "./common";
import { Marginer } from "../../marginer/marginer";
import { AccountContext } from "./accountsContext";

function SignupForm({ signup, isAuthenticated }) {
  const { switchToLogin } = useContext(AccountContext);
  const [accountCreated, setAccountCreated] = useState(false);
  const [formData, setFormData] = useState({
    first_name: "",
    last_name: "",
    birthday: "",
    email: "",
    password: "",
    re_password: "",
  });

  const { first_name, last_name, birthday, email, password, re_password } =
    formData;
  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });
  const onSubmit = (e) => {
    e.preventDefault();

    if (password === re_password) {
      signup(first_name, last_name, birthday, email, password, re_password);
      setAccountCreated(true);
    }
  };

  if (isAuthenticated) {
    return <Navigate to="/" />;
  }

  if (accountCreated) {
    return <Navigate to="/checkemail" />;
  }

  return (
    <BoxContainer>
      <FormContainer onSubmit={(e) => onSubmit(e)}>
        <Input
          type="text"
          placeholder="First Name"
          name="first_name"
          value={first_name}
          onChange={(e) => onChange(e)}
          required
        />
        <Input
          type="text"
          placeholder="Last Name"
          name="last_name"
          value={last_name}
          onChange={(e) => onChange(e)}
          required
        />
        <Input
          type="text"
          placeholder="Birthday"
          name="birthday"
          value={birthday}
          onChange={(e) => onChange(e)}
          onFocus={(e) => (e.target.type = "date")}
          required
        />
        <Input
          type="email"
          placeholder="Email"
          name="email"
          value={email}
          onChange={(e) => onChange(e)}
          required
        />

        <Input
          type="password"
          placeholder="Password"
          name="password"
          value={password}
          onChange={(e) => onChange(e)}
          minLength="8"
          required
        />
        <Input
          type="password"
          placeholder="Confirm Password"
          name="re_password"
          value={re_password}
          onChange={(e) => onChange(e)}
          minLength="8"
          required
        />
        <Marginer direction="vertical" margin={10} />
        <SubmitButton type="submit">Signup</SubmitButton>
      </FormContainer>
      <Marginer direction="vertical" margin="1em" />
      <MutedLink>
        Already have an account?{" "}
        <BoldLink onClick={switchToLogin} style={{ color: "#df6c49" }}>
          Login
        </BoldLink>
      </MutedLink>
    </BoxContainer>
  );
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { signup })(SignupForm);
