import React, { useState, useContext } from "react";
import { Navigate } from "react-router-dom";
import { connect } from "react-redux";
import { login } from "../../actions/auth";
import { Marginer } from "../../marginer/marginer";
import { AccountContext } from "./accountsContext";
import {
  BoldLink,
  BoxContainer,
  FormContainer,
  Input,
  MutedLink,
  SubmitButton,
} from "./common";

function LoginForm({ login, isAuthenticated }) {
  const { switchToSignup, switchToReset } = useContext(AccountContext);
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  const { email, password } = formData;
  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });
  const onSubmit = (e) => {
    e.preventDefault();

    login(email, password);
  };

  if (isAuthenticated) {
    return <Navigate to="/" />;
  }

  return (
    <BoxContainer>
      <FormContainer onSubmit={(e) => onSubmit(e)}>
        <Input
          type="email"
          placeholder="Email"
          name="email"
          value={email}
          onChange={(e) => onChange(e)}
          required
        />

        <Input
          type="password"
          placeholder="Password"
          name="password"
          value={password}
          onChange={(e) => onChange(e)}
          minLength="8"
          required
        />
        <Marginer direction="vertical" margin="1.6em" />
        <SubmitButton type="submit">Login</SubmitButton>
      </FormContainer>
      <Marginer direction="vertical" margin={10} />
      <MutedLink>
        <BoldLink onClick={switchToReset} style={{ color: "#df6c49" }}>
          Forgot your password?
        </BoldLink>
      </MutedLink>
      <Marginer direction="vertical" margin="1em" />
      <MutedLink>
        Don't have an account?{" "}
        <BoldLink onClick={switchToSignup} style={{ color: "#df6c49" }}>
          Sign Up
        </BoldLink>
      </MutedLink>
    </BoxContainer>
  );
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { login })(LoginForm);
