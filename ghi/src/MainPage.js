import "./App.css";
import HeroSection from "./HeroSection";
import React from "react";
import AllCards from "./AllCards";
import Footer from "./Footer";

function MainPage() {
  return (
    <>
      <div className="MainPage"></div>

      <HeroSection />
      <AllCards />
      <Footer />
    </>
  );
}

export default MainPage;
