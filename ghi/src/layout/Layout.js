import React, { useEffect } from "react";
import Navbar from "../Nav";
import { connect } from "react-redux";
import { checkAuthenticated, load_user } from "../actions/auth";


function Layout(props) {
  useEffect(() => {
    checkAuthenticated();
    load_user();
  }, []);

  return (
    <div>
      <Navbar />
      {props.children}
    </div>
  );
}

export default connect(null, { checkAuthenticated, load_user })(Layout);
