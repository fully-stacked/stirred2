## 09/14/2022

Today,

- More css
- Added search function to home page implementing our 3rd party API
- Worked with Jonathan to take the state from my search func and make cards out of the responses

Aha:

Paired programming: With Jonathan after I finished my stuff

## 09/13/2022

Today,

- Had to write a function to validate the fields in the form so the form switch would happen when the fields are correctly filled
- Tried to help figure out a bug with kieran and menger, phil had to save us

Aha: Nothing today

Paired programming: during debug time

## 09/12/2022

Today,

- Got stuck on this bug where forms were changing by just hitting the submit button, even if you accidentally clicked it, this bug destroyed me but I fixed it
- finished steps form with a stylish link to go back to ingredients if you missed one

Aha: Putting condition statement in my onClick handler with setting state from false to true onSubmit fixed my problem, I will never forget that trick now lol

Paired programming: no

## 09/09/2022

Today, forgot to do this one

- We merged all together to set us up for the weekend, but now in hindsight, I didn't have much time to work on the project over the weekend
- Spent most of my time trying to fix a bug where the initial state wasn't being set properly

Aha: Starting to get the hang of useEffect now

Paired programming: No paired programming today

## 09/08/2022

Today,

- We realized that we forgot a field on one of our forms, so I added it to a new page with seamless animations to make it look like one page while triggering necessary useEffect functions to get the data of the current user and the current user's most recent drink
- Had one bug that took some brainpower to fix, I had to find a way to get the current user's ID and their drink ID to add to the form to add ingredients

Aha:

Paired programming: no paired programming today

## 09/07/2022

Today,

- With the help of Daniel, we resolved our insane merge conflicts that ate up about 2 hours
- Got working NewDrinkForm code from Menger and styled it to match the login/signup form
- started styling our reset-password page to match NewDrink and login/signup

Aha: React hooks are a godsend. We cannot use the animation in class based components because we need to use hooks

Paired programming: No paired programming today

## 09/06/2022

Today,

- Styled the New Drink page and included an animation similar to the one I did for login

Paired programming: No paired programming today

## 09/05/2022

On this Labor Day,

- Added animations and styling to login/signup page while maintaining functionality

Aha: animations aren't that difficult, just tedious

Paired programming: no paired programming

## 09/02/2022

Today,

- We only had an hour so we all merged into test branch so we have updated build to work on during the weekend

Aha:

Paired programming: We all helped each other merge

## 09/01/2022

Today,

- Added our user into our existing models and adjusted views accordingly
- dealt with nasty bug when testing CI but solved it

Aha: dj_database_url is extremely useful because it adapts to the CI user and password as well as in our compose file

Paired programming: No paired programming today

## 08/30/2022

Today,

- We made ALOT of progress today, no journal entries for the past few days because they have been hectic and we decided to switch to django+Postgres so we're trying to play catch-up
- Drink, Step, Review, Ingredients models and views are all complete and working as intended

Aha: We shouldn't have used MongoDB in the first place (I suggested it :( ) because we have alot of relationships that Postgres is helping with

Paired programming: Menger and I were working together on the backend.

## 08/29/2022

## 08/24/2022

Today,

- We all worked together to get allow the ingredients field in the Drinks document to allow multiple ingredients

Aha: We probably shouldve used Django

## 08/23/2022

Today,

- Finished ingredients CRUD functionality
- Discussed how we need to clean up our code and delete the dead code

Aha: We might've been too ambititous in the amount of models we wanted to have.

Paired programming: After finishing the ingredients models, I joined the group and we finished ~80% of Drinks models and all of Reviews models together.

## 08/22/2022

Today,

- Got ingredients model and routers 80% complete
- Discussed relationships between models and how we need to change them

Aha: It's harder to start projects because you may need some info that someone else is working on, so it takes more communication than I thought.

Paired programming: We all came together to do some of the Drinks model and router functions because we all kind of need that functionality to continue.

## 08/19/2022

Today,

- We got docker-compose.yaml file to work as we expected
- Discussed the relationships between our models and fixed them accordingly (still not completely done)
- Discussed ways to format our database because we are using MongoDB

Aha: Many of the headaches of SQL table creation are non existant in Mongo, but you still must be aware of how you're storing data.

Paired programming: We all worked together
