# 14SEP22

-Today we focused on implementing our API key within our project. I spent a good chunk of this morning learning/researching how to properly insert the API key, and I will admit that I found it more than a little nerve-wracking to think of the possibility that I publicly display the API key, and get charged hundreds/thousands of dollars by complete strangers. Yikes... Afternoon was spent coming up with several unit tests for my portion of the project. 

-My aha moment for the day was learning just how serious hiding your API key really is. I will admit that if it were left entirely up to me, I'd be seriously looking for someone to proofread, before I submitted LOL I suppose the solution is to hide the API key in your .gitignore file? From what I understand.

## 13SEP22

-Today we worked as a group, toinclude Phil, on a 5 star rating system for the drinks in our app. It was a very long process, but in the end, Phil gave a great teaching moment on parent-child relationships in react.

-My aha moment today was as follows:

VS Code shortcut to create a file within a folder:
Create new file, with name "foldername/filename.js(html, py, etc)"

When you hit enter, it will create the folder, with file imbedded


## 12SEP22

-Today I spent the majority of my time working on cookies. We are trying to add them into our project to track when the user is logged in. I am currently in the research phase, and will soon start implementing actual code. 

-Today's aha moment was being able to fix a red squiggly error I was having on my Footer.js page. The original issue was written as "export default Footer;". Working with the rest of the group to solve it on friday lead nowhere. Today, I FIXED IT. I ended up erasing it, and rewriting the same code in again, and it worked. Why? Don't ask. I noticed that the colors of the letters were all jumbled up, however. DE was a different color than FAULT, which I had never seen before, so I tried rewriting it. Boom. It worked. 

## 08SEP22

-Today I managed to get working links for the app footer. This was honestly much easier than I made it out to be. After countless YT videos and StackOverflow articles(even bootstrap), I ended up simply needing to imbed an href into an li tag. Face palm. Now I know. 

-My aha moment today was honestly just discovering how much I was overthinking the link in my footer. Sometimes the simplest solution is right in front of you. I think I'm just getting a little burned out :) 

## 07SEP22

-Today we spent just about 2 hours getting our testmain fully merged and up to date with everyone's code. I spent the majority of my time afterwards getting a working footer for our app. I have the footer working, and tomorrow I will be aiming to replace the current footer text with working links. 

-My aha moment came when we collectively realized we are going to have to merge our work much more frequently. With needing Daniel's help, and still spending over 2 hours to get every merge working properly, this will significantly block us, if we continue to operate this way, moving forward. 

## 06SEP22

-Today we divided up to finish the homepage and create a drink page. Jason worked on a search bar feature, for the home page, and will work with Jonathan and Malik to incorporate that with the home page. 

-My aha moment today was learning that // will give the rounded down division. For example 5 //2 will give you 2(down from 2.5).

## 02SEP22 

-Everyone in the group merged their portion of code into a new "testmain" file, and got that completely up to date/running. Not much time for anything else, simply due to Mandatory Fun. Plan to work over the weekend for a bit. 

-My aha moment today was figuring out how to successfully merge everyone's files into the testmain file. 

## 01SEP22

-Malik and Menger continued to work on incorporating the User model into the back end, setting up Insomnia requests/making dummy data. Jason, myself, and Jonatahn all worked a bit on the front-end side of things. I am currently trying to display all drinks, but need to first create a drink. 

-My aha moment came while earning about Algorithms and how to loosely figure out what is being talked-about. 

## 31AUG22

-We continued to work on separate pieces today. Jason and myself were working on the front-end; Jason specifically on User-Auth. Malik, Jonathan, and Menger continued to work on the back-end, finializing the modules. 

-My aha moment was theentire lecture on React hooks, and learning how the chat feature works. 

## 30AUG22

-Made a good bit of progress on the front-end today. I made skeleton pages for Listing the drinks, Favorited drinks, and creating a new drink. Jason made great progress on User Auth, getting that set-up to email new users. 

-My aha moment was discovering the VS Code extension "Prettier," to help with formatting. 

## 26AUG22

-Today we met for a brief stand-up and decided to forgo completing this project using FastAPIs, and use Django models instead. We spent the morning translating our files from our first project iteration into our new project "Stirred2".

-My aha moment happened during our stand-up, when I realized we actually have much further to go with our FastAPI knowledge than I had considered. 

## 25AUG22

-Today, we have focused on making our CI/CD files, and getting the project deployable. Furthermore, we continued to work on our Reviews model. 

-My aha moment  came when learning how to write my first test.

## 24AUG22

-Today, we have been continuing to work on our Ingredients model. There has been some hang-up, with regaards to how to implement the Ingredients model with MongoDB.

-My aha moment was reiterating the difference between "int" (a number without a decimal point) and "float" (a number with a decimal point)

## 23AUG22 

-Today, we worked on setting up our FastAPI endponts for our Reviews model

-My aha moment came when Andrew showed me that using the ctrl + "~" button, simultaneously, will 