## September 15, 2022
Today, I worked on:
    - drink detail page for db
    - results page based on search for main page
    - deployment

Had some issues with deployment with flake8 not liking some blank lines that were done. I worked on getting that fixed up. The drink detail page from the day before were huge but working with Malik we were able to get that all fixed up. Took the skeleton from the other drink detail page and used that. After deploying, we started testing and ran into a few small issues with the description of adding a new drink if its too many works it won't work but the site won't tell you why. that a small bug we have to squash. Also some of the css styling needs some small tweaks. Couldn't figure out how to get an API key to only work on the front-end through the gitlab CI/CD variables. 

AHA moment was when the deployment worked with all of the variables masked.
## September 14, 2022
Today, I worked on:
    -drink detail page for other DB
    -drink cards from other db

The secondary DB that we are using requires a different access format compared to the one we fully built ourselves. We had to add a new .env file into the root of the project to add the new API key. I had to add that to the .gitignore and think about how to put the var into gitlabs CI/CD variables. Going to work on that tomorrow. I got the cards to show up on the home page however the drink ID is not passing into the drinks detail page. need to check on the state and see how to pass it over properly.

AHA moment was when the cards actually worked and the parent child components were talking to each other. I was excited because i couldn't test if any of these features worked until they were all built out.
## September 13, 2022
Today, I worked on:
 - deployment
 - CSS styling and search bar

 The deployment was still giving me issues today; however I figured out the issue. The deployment seems to be completely functioning and working as intended. The error was from a env variable missing on the heroku side. I just had to add in the gitlab.io link for our project there and it started working. Same with a env var missing on the gitlab side for the gitlab io var.

 The AHA moment today was figuring out that Heroku also needed to have the link for gitlab for it to be able to connect to gitlab pages.

## September 12, 2022
Today, I worked on:
- deployment
- new drinks page

Have issues with the deployment backend with it not being able to create a superuser and access an admin page. The new drinks form i still need to grab the user id from the jwt config to send to the api and than the drinks form will be complete.

## September 8, 2022
Today, I worked on:
- create new drinks page
- drink details

I worked to add a reviews sections with cards on the bottom of the drink details. There was an issue with the spacing on the review cards that still needs to be worked out. I also want to add in a system for converting the numerical value of the reviews into a star system. I also worked on the new drinks page and converting it from a class based react view to a functional one. I had to make some choices to fix the categories.

## September 7, 2022
Today, I worked on:
- Merge conflicts
- DrinkDetails

Worked with daniel to sort out the git conflicts with the team. Worked on the DrinkDetails page and redoing imports to fit new folder structure. Drink Detail is more difficult than I initially predicted with having to dynamically display information based on which drink id is coming in from the AllDrinks page.

AHA moment was learning some new git commands.

## September 6, 2022
Today, I worked on:
-CD

Found out from a teammate that the gitlab deployed pages wasn't talking properly with the backend on heroku. Tried to figure out what was wrong. If the front-end also has to be on heroku its an issue with the ghi/public not being copied over in the scripts of the ci-yml file. Couldn't work it out and worked on the DrinkDetails page instead.


## September 5, 2022
Today, I worked on:
-Continuous Deployment

Today, I worked on the CD. It was a bit of the struggle to figure out what exactly the gitlab syntax was so for some of the ci-yml so I had to go read some of the documentation to get an understanding. The CD is currently working in its current state but having some confusion about the heroku deployment. Is it purely backend or both front and back.

AHA moment was when the final deploy worked and the containers were sent over to the Heroku servers.
## September 2, 2022
Today, I worked on:
-NAVBAR
-NAVBAR CSS

Today, I worked on styling the CSS and getting some more skeleton frames of pages setup. It was hard to get the logo with the css to be centered properly when in desktop mode vs the mobile mode. It is currently centered on desktop mode but it is not on the mobile side. It is off centered.

## September 1, 2022
Today, I worked on:
-HOME page
-CD

Got the home page up and running with a Hero container and a video playing in the background(looped). Have cards at the bottom of the page so that people can try out curated drinks. Need to think about resizing images or see how to do that in the css for the cards to limit the image size.
## August 31, 2022
Today, I worked on:
-React front end got a nav bar up and running and the drink details page up and running. Waiting for backend completion to plug in the data dynamically.

AHA moment is when I got the columns and the row working properly and they were spaced properly.

## August 30, 2022
Today, I worked on:
-Continuous Deployment

I looked into the CD cookbook and set about setting it up on HEROKU. I have the API from heroku added into the gitlab CI/CD variables. Just waiting on the back end to be finished to be able to decide which files and folders will need to be copied as the structure is changing still.

AHA moment was when I had a merge conflict and it wouldn't be resolved.
## August 29, 2022
Today, I worked on:
- the REACT front end and the postgres DB

We worked more individually on our own individual parts that we claimed on the SCRUM board.

AHA moment was when the DJANGO DB connected to the POSTGRES DB after doing some bug fixing. MY teammates are unable to recreate how I was able to connect despite have the same process.

## August 25, 2022
Today, I worked on:
    -more of the MongoDB back end and inserting more subdocuments
    -deciding tomorrow if we can't figure out mongo and fastAPI we should make the switch to Django

AHA moment is when I figured out how to insert a subdocument into another subdocument
## August 24, 2022
Today I worked on:
- the ingredients model and the POST, GET methods

We worked as a group again to get the ingredients model to work with the Drinks model. We ran into issues again with the database schema and had to invite a few SIERS in to discuss how it should be done. The advice given was similar to what i had in mind with using sub-documents on the drinks model for the ingredients. This resolves the issue of not having to create a separate model with the measurements and amounts. It did takes us a long while to really get the logic down in our heads especially with an unfamiliar DB structure. 

The AHA moment was when we inputted just the ingredient ID, inside Mongo works some magic and populates the documents with the data from the ingredients schema.

## August 23, 2022

Today, I worked on:
- the reviews models and the GET, POST, and DELETE methods

Again today, we worked as a group on the reviews model and while we did run into a few error with error 500 response from the server. We were able to work out the kinks with the assistance of a SEIR. The original thought was that reviews should go as a subdocument inside of the drinks document. But that turned out to be difficult and showed that I needed to read more on the mongo documentation. Currently we just use a separate document and use find and lookup to point to other documents.

Aha moment today was when the reviews finally got working and was posting correctly.

## August 22, 2022

Today, I worked on: 

- Creating the drinks models and the GET and POST methods
- Creating the Reviews models and the GET and POST methods

We worked as a group on the Drinks model with everyone on John's branch and the Reviews model I worked in a paired session with Menger to get the GET and POST methods functioning. We did run into issues and its become clear that we have to have a clear schema for the MONGODB tables or else it's going to get really difficult to create all the methods needed. 

I had a great aha moment when thinking about the drinks schema and how the reviews should actually be a subdocument to drinks rather than a separate document. It just makes more sense for the data to be store like this.

## August 19, 2022

Today, I worked on:

- Creating the Dockerdev files for the drinks API
- Creating the Docker Compose YAML File
- Got the REACT front-end to show the default LOGO!!!

We had a deep conversation with the team and Daniel in terms of how the database structure would work with Mongo and how to frame the recipes as a documents within Mongo. We designed a basic ERD which currently still needs some work in terms of how tables should be relating to each other.

On our first try on docker build and compose, our code worked with minimal error which was personally a big accomplishment today. Getting a nice blank slate framework working make the incoming task just a little bit easier.
