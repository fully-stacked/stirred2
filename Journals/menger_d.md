## 09/14/2022
Today I focused on CSS within our project. I spent a good chunk of time last night and this morning creating a logo, started styling our pages and added animations and styling to detail page while maintaining functionality


## 09/13/2022
Still working on the frontend part for create review, since add reviewstar components, can not point out how to get the starts value into the state and send to backend. Merge together at the end of the day. Created couple of logos to share with team tomorrow.

## 09/12/2022
Working on frontend pages,add css into drink detail page. Try to use review star to show the rating, still working on that. We merged all together to set us up for the following days. Aha moment for today, I used grid is css to seperate the reviews.Almost forget how to use that, after review some videos from YT, I got the idea and tried it out.

## 09/07/2022
Everyone in the group merged their portion of code into a new "testmain" file, and got that completely up to date/running. With the help of Daniel, we resolved our merge conflicts that ate up about 2 hours.

## 09/06/2022
Today we divided up to finish the homepage and create a drink page. And try to create new review feature for the detail page.

## 09/02/2022
Discussed frontend pages, and start to work on my part, creating a new drink page, using react. Getting learn how to use authentication. Trying to add coockies into our project to track when the user is logged in. Still working on that. 

## 09/01/2022
Almost finish backend, merged with user model and authentication. We can successfully work for creating users. Spent time on setting frontend for next week's diving. 

## 08/31/2022
Merge Malik's branch for all models, views and urls, all the request methord work well. Wrote test case for model. Will add more test case in the following days. Team member worked on user authentication for login.Tomorrow will work on frontend pages.


## 08/30/2022
Spent time on models, views and url to build api endpionts. From all day working on those, I realized that when we want to change or add a feature, it will totally cause a lot of changes in other models and also affect the whole database. 

## 08/29/2022
We trid to merge the work from weekends but found some issue, we forgot the gitignore file, spent most of the day on that, did a lot of rebase commands, and finally, I got lost on my branch. It shows that my local branch is ahead of the main branch. We coule not fix that by our knowledge so far and also the database was not able to connect.

## 08/28/2022
During the weekends, I recap the previous projects to warm up django framework. And wrote review model, step model, and ingredient item models. Also, I wrote a draft view.py file for those models. Since many of my models need to combine with team’s models because of the foreign keys, I will make some change when team meet on Monday.


## 08/26/2022
After 3 days stuck with fastapi, getting hard to move forward, our group finally switched to django framework. This morning we met together to finish the docker.compose. yaml file, and our app got successful running. Everyone feels satisfied now. 


## 08/22/2022

Finally we started editing code today, with the concept of relationships between models, I created a review model, and built the router for review. As the first project using fastapi, I still feel challenged to complete the whole part. Will spend more time on that with groups. But definletly I from today's practice I am getting more confident in fastapi than I did on yesterday.

## 08/19/2022

Disccussed and established docker-compose.yml with other group members.That was really an Aha momment. Upgrade the relationships between our models to make them more reasonable. Finally we got the framwork og the project.

## 08/17/2022

Disccussed our models and made a draft on that ,updated our README after receiving feedback from instructor. Went through project design as a group and made sure we are on the same page. Keep some idea as stretch goal.

## 08/16/2022

Today was mostly just making sure everyone was on the same page and what tech stack we were agreeing to use. The topic of contention was back-end implementation using fastapi or django and we ultimately decided on fastapi. I’m ot sure with that decision because fastapi is very fresh for me and I still need more time to go along with it. But on the other hand, it will be agood challenge for all of us.

## 08/15/2022

Our group ended up choosing cocktail recipe as the topic decided to create an application for a digital cocktail companion that allows user to select available ingredients from  kitchen/bar and generate cocktails that can be made from those ingredients.Mostly we just tried to talk about goals and finish the part of readme. At this point, I’m still a little foggy on how models are relateing to each other but I have enough of an understanding to start moving forward.



