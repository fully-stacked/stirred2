## 09/15/22
Today I:
- Fixed search on all drinks and mydrinks

## 09/14/22
Today I:
- Added masonry style to drinks and all drinks page

## 09/13/22
Today I:
- Realized that we had to put the environment variable into Gitlab CI/CD config variables. We then got a CORS related issue and realized that we had to put CORS_HOST into Heroku config variables

## 09/12/22
Today I:
- Worked on current user created drinks
- Trying to work on deployment and wondering why the client can't get the proccess environment variable.

## 09/08/22
Today I:
- Created a search bar for all drinks and home page

I realized I had to use 'useNavigate' and 'useLocation' to pass state from page to another page.

## 09/07/22
Today I:
- Successfully merge into testmain and have all group member files updated
- Finished the all drinks page

## 09/06/22
Today I:
- I started working on the list all drinks page
- Helped a team memeber with assigning current user id to the create a drink page

## 09/01/22
Today I:
- Finished the fronend for all the user registration such as: login, signup, reset password, and account verification.

## 08/31/22
Today I:
- Finished the authentication and login on the frontend

I will be working on the logout, reset password, and sign up page tomorrow.

## 08/30/22
Today I:
- Finished the backend for accounts and users with an automated email system.
- Figured out that our .sh files needed to be in LF instead of CRLF in order to run properly

## 08/24/22
Today we:
- Pair programmed for review queries and routers

We got stuck on trying to nest reviews within the drink models, but we figured out that we didn't neeed to do that. We could just make a new review model refrencing the drink_id.

## 08/22/22
Today we:
- I wrote began writing the models, routers, and queries for users.
- We paired programmed for drinks, but we are getting an error and are still working on the drink routes.

FastAPI is starting to make a little more sense to me as I am using it more.

## 08/19/22

Today we:
- Wrote our docker-compose.yaml file
- Started our react app

We had minimal problems getting our docker and react to work, which was great.