# stirred2

## Design

- [API design](docs/apis.md)
- [Data model](docs/data-model.md)
- [GHI](docs/ghi.md)
- [Integrations](docs/integrations.md)

## Team Members

1. John Eger
2. Jonathan Chen
3. Jason Nguyen
4. Malik Omar
5. Menger Dou

## Summary

Digital Cocktail companion that allows you to select available ingredients from your kitchen/bar and generate cocktails that can be made from those ingredients.

## Intended Market

Stirred is a project that is ideal for:

- People that want to create and share their custom cocktails
- People or businesses that want to make cocktails on the fly with their available ingredients
- People who want to try and make a cocktail that is listed already

## MVP

- login/sign-up with auth
- See listed recipes
- Users can create their own drinks and share them
- Users can search by an ingredient and see a list of cocktails they can make based on the ingredient inputted
- Filter by different categories of drinks

## Website

[STIRRED](https://fully-stacked.gitlab.io/stirred2/)

## Stretch Goals

- Premium account
- If a user doesn't have a necessary ingredient, they can see the nearest liquor store that has that item in stock
- Multi ingredient search
