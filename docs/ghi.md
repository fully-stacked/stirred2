# Graphical Human Interface

## Home Page

[Home Page](/docs/wireframe/homepage.png)

## Login / Signup Page

[Login Page](/docs/wireframe/login.png)

## Search Results and Filter

[Search Results](/docs/wireframe/search-results.png)

## Account Profile

[Account Profile](/docs/wireframe/acct-profile.png)
