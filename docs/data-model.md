## IngredientItem

| Name        | Type                 | Unique | Optional |
| ----------- | -------------------- | ------ | -------- |
| name        | string               | no     | no       |
| amount      | float                | no     | no       |
| Measurement | string               | no     | yes      |
| drink_id    | ForeignKey to drinks | no     | no       |

## Category

| Name | Type   | Unique | Optional |
| ---- | ------ | ------ | -------- |
| name | string | yes    | no       |

## Step

| Name       | Type         | Unique | Optional |
| ---------- | ------------ | ------ | -------- |
| order      | posSmallInt  | no     | no       |
| directions | string       | no     | no       |
| drink_id   | FK to drinks | no     | no       |

## Drinks

| Name        | Type           | Unique | Optional |
| ----------- | -------------- | ------ | -------- |
| name        | string         | no     | no       |
| alcoholic   | boolean        | no     | no       |
| description | string         | no     | yes      |
| picture_url | URL            | no     | yes      |
| category    | FK to category | no     | no       |
| creator     | FK to user     | no     | no       |

## Reviews

| Name    | Type                | Unique | Optional |
| ------- | ------------------- | ------ | -------- |
| rating  | int (1-5)           | no     | no       |
| comment | text                | no     | no       |
| drink   | ForeignKey to drink | no     | no       |

## Users

| Name       | Type   | Unique | Optional |
| ---------- | ------ | ------ | -------- |
| birthday   | date   | no     | yes      |
| is_active  | bool   | no     | no       |
| first name | string | no     | no       |
| last name  | string | no     | no       |
| email      | string | yes    | no       |
| is_staff   | bool   | no     | no       |
