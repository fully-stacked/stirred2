thecocktailDB -

# APIs

## Getting a drink

- **Method**: `GET`
- **Path**: /api/drinks/{id}/

Input:

```json
{
    "name": string,
    "ingredients": int,
    "category": string,
    "glasstype": string,
    "alcoholic" : boolean,
}
```

Output:

```json
{
    "drinks":[{
        "name": string,
        "alcoholic": Boolean,
        "strInstructions": string,
        "strDrinkThumb": string,
        "strIngredients1": string,
        "strMeasure": string,
        "dateModified": datetime

    }]
}
```

## Creating a drink

- **Method**: `POST`
- **Path**: /api/drinks/

Input:

```json
{
    "name": str,
    "alcoholic": bool,
    "category": string,
    "tags": string,
    "description": string,
    "picture_url": string,
    "ingredients": string,
    "steps": string
}
```

Output:

```json
{
    "id": int,
    "alcoholic": bool,
    "category": string,
    "tags": string,
    "description": string,
    "picture_url": string,
    "ingredients": string,
    "measurements": string,
    "steps": string



}
```

Creating a drink uses the incoming name data to query an image api to get an URL for an image of the drink if an URL is not provided. Then it saves the name, alcoholic, category, tags, description, ingredients, picture_url, and steps to the database. It returns all of the data with the new database id.

## Getting drink list

- **Method**: `GET`
- **Path**: /api/drinks/

Input:

```json
{
    "name": string,

}
```

## Users List

- **Method**: `GET`
- **Path**: /api/users

## Get a user

- **Method**: `GET`
- **Path**: /api/users/{id}/

Input:

```json
{
    "id": int,
}
```
