## Initial Feedback 

### Project idea:
This is an interesting idea, especially the "what can I make with what I have on hand?" part. There is no shortage of folks who might want to you your site. Have you given any thought about how you would get users to come to the site and start contributing recipes? 

### Scope of work
The scope of work is reasonable, but you haven't differentiated your MVP features from your stretch-goals. Doing so not only gives you a safety buffer as the end of the project nears, but will, more importantly, help you focus on the most important parts of your application first.

### Docs
The start on your docs looks good. For your benefit, keep those up-to date as you make decisions and refine your designs. Use your `integrations.md` file to keep track of any useful information that you find about setting-up and configuring the integrations. These can be a pain to figure out sometimes, document it so it is easy for your other team members and for you later when you forget... If you all use these docs as THE place that you put any and all documentation about your project, it will be easier for you all to stay on the same page.

I recommend that you create wireframes for your pages before you start creating them.
