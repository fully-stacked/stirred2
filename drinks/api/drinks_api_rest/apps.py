from django.apps import AppConfig


class DrinksApiRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'drinks_api_rest'
