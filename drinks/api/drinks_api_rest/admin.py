from django.contrib import admin

# Register your models here.
from .models import (
    Drink,
    Step,
    Review,
    IngredientItem
)


class DrinkAdmin(admin.ModelAdmin):
    pass


class StepAdmin(admin.ModelAdmin):
    pass


class ReviewAdmin(admin.ModelAdmin):
    pass


class IngredientItemAdmin(admin.ModelAdmin):
    pass


admin.site.register(Drink, DrinkAdmin)
admin.site.register(Step, StepAdmin)
admin.site.register(Review, ReviewAdmin)
admin.site.register(IngredientItem, IngredientItemAdmin)
