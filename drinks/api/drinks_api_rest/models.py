from django.db import models
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from django.urls import reverse
from accounts.models import UserAccount

USER_MODEL = settings.AUTH_USER_MODEL


class Category(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class Drink(models.Model):
    name = models.CharField(max_length=25)
    alcoholic = models.BooleanField(default=False)
    description = models.CharField(max_length=200, null=True)
    picture_url = models.URLField()
    creator = models.ForeignKey(
        UserAccount,
        related_name="drinkUser",
        on_delete=models.CASCADE
    )
    category = models.ForeignKey(
        Category,
        related_name="drink",
        on_delete=models.PROTECT
    )


class Step(models.Model):
    drink = models.ForeignKey(
        "Drink",
        related_name="steps",
        on_delete=models.CASCADE,
    )
    order = models.PositiveSmallIntegerField()
    directions = models.CharField(max_length=300)

    def get_api_url(self):
        return reverse('api_step', kwargs={'pk': self.pk})

    def __str__(self):
        return str(self.order) + ". " + self.directions


class Review(models.Model):
    rating = models.PositiveSmallIntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1),
        ]
    )
    creator = models.ForeignKey(
        UserAccount,
        related_name="ratings",
        on_delete=models.CASCADE
    )
    drink = models.ForeignKey(
        "Drink",
        related_name="ratings",
        on_delete=models.CASCADE,

    )
    comment = models.TextField()


class IngredientItem(models.Model):
    name = models.CharField(max_length=100)
    amount = models.FloatField(validators=[MinValueValidator(0)])
    measurement = models.CharField(max_length=25, blank=True)
    drink = models.ForeignKey(
        "Drink",
        related_name="ingredient",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name
