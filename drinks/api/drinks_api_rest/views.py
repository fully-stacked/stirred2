import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from accounts.models import UserAccount
from .models import Drink, IngredientItem, Review, Step, Category
from .encoders import (
    DrinkEncoder,
    StepListEncoder,
    StepDetailEncoder,
    ReviewListEncoder,
    ReviewDetailEncoder,
    IngredientListEncoder,
    IngredientDetailEncoder,
    CategoryListEncoder,
    UserListEncoder
)


@require_http_methods(["GET", "POST"])
def api_users(request):
    if request.method == "GET":
        users = UserAccount.objects.all()
        return JsonResponse({"users": users}, encoder=UserListEncoder)


@require_http_methods(["GET", "POST"])
def api_drinks(request):
    if request.method == "GET":
        drinks = Drink.objects.all()
        return JsonResponse({"drinks": drinks}, encoder=DrinkEncoder)
    else:
        content = json.loads(request.body)
        try:
            content['category'] = Category.objects.get(id=content['category'])
            content['creator'] = UserAccount.objects.get(id=content['creator'])
        except Category.DoesNotExist:
            return JsonResponse(
                {"message": "Category does not exist"},
                status=400
            )
        drinks = Drink.objects.create(**content)
        return JsonResponse(
            drinks,
            encoder=DrinkEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_drink(request, drink_id):
    if request.method == "GET":
        drink = Drink.objects.get(id=drink_id)
        return JsonResponse(
            drink,
            encoder=DrinkEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        try:
            count, _ = Drink.objects.filter(id=drink_id).delete()
            return JsonResponse({"deleted": count > 0})
        except Drink.DoesNotExist:
            return JsonResponse(
                {"message": "Drink cannot be deleted"},
                status=400
            )
    else:
        content = json.loads(request.body)
        try:
            if 'category' in content:
                content['category'] = Category.objects.get(id=content['category'])
            Drink.objects.filter(id=drink_id).update(**content)
            drink = Drink.objects.get(id=drink_id)
            return JsonResponse(
                drink,
                encoder=DrinkEncoder,
                safe=False
            )
        except Drink.DoesNotExist:
            return JsonResponse(
                {"message": "Drink cannot be updated"},
                status=400
            )


# delete later--->
@require_http_methods(["GET"])
def api_all_reviews(request):
    reviews = Review.objects.all()
    return JsonResponse(
        {"reviews": reviews},
        encoder=ReviewListEncoder,
    )
# <-----


@require_http_methods(['GET', 'POST'])
def api_reviews(request, drink_id):
    reviews = Review.objects.filter(drink=drink_id)
    if request.method == 'GET':
        return JsonResponse(
            {'reviews': reviews},
            encoder=ReviewListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            content['drink'] = Drink.objects.get(id=drink_id)
            content['creator'] = UserAccount.objects.get(id=content['creator'])
        except Drink.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid drink id'},
                status=400,
            )

        review = Review.objects.create(**content)
        return JsonResponse(
            review,
            encoder=ReviewDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "PUT"])
def api_review(request, pk):
    if request.method == 'DELETE':
        try:
            count, _ = Review.objects.filter(id=pk).delete()
            return JsonResponse({"delete": count > 0})
        except Review.DoesNotExist:
            return JsonResponse(
                {"message": "Review be deleted"}
            )
    else:
        content = json.loads(request.body)
        try:
            Review.objects.filter(id=pk).update(**content)
            review = Review.objects.get(id=pk)
            return JsonResponse(
                review,
                encoder=ReviewDetailEncoder,
                safe=False
            )
        except Review.DoesNotExist:
            return JsonResponse(
                {"message": "Review cannot be updated"},
                status=400,
            )


@require_http_methods(['GET', 'POST'])
def api_ingredient_items(request, drink_id):
    if request.method == 'GET':
        ingredients = IngredientItem.objects.filter(drink=drink_id)
        return JsonResponse(
            {'ingredients': ingredients},
            encoder=IngredientListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            content['drink'] = Drink.objects.get(id=drink_id)
        except Drink.DoesNotExist:
            return JsonResponse(
                {"message": "Could not add ingredient, drink does not exist"},
                status=400
            )
        ingredient = IngredientItem.objects.create(**content)
        return JsonResponse(
            ingredient,
            encoder=IngredientDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "PUT"])
def api_ingredient_item(request, pk):
    if request.method == 'DELETE':
        try:
            count, _ = IngredientItem.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except IngredientItem.DoesNotExist:
            return JsonResponse(
                {"message": "Ingredient cannot be deleted"}
            )
    else:
        content = json.loads(request.body)
        try:
            IngredientItem.objects.filter(id=pk).update(**content)
            ingredient_item = IngredientItem.objects.get(id=pk)
            return JsonResponse(
                {"ingredient": ingredient_item},
                encoder=IngredientDetailEncoder,
                safe=False
            )
        except IngredientItem.DoesNotExist:
            return JsonResponse(
                {"message": "Ingredient cannot be updated"},
                status=400
            )


@require_http_methods(['GET', 'POST'])
def api_steps(request, drink_id):
    if request.method == 'GET':
        steps = Step.objects.filter(drink=drink_id)
        return JsonResponse(
            {'steps': steps},
            encoder=StepListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            content['drink'] = Drink.objects.get(id=drink_id)
        except Drink.DoesNotExist:
            return JsonResponse(
                {'message': 'Could not add step, drink does not exist'},
                status=400,
            )
        step = Step.objects.create(**content)
        return JsonResponse(
            step,
            encoder=StepDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "PUT"])
def api_step(request, pk):
    if request.method == "DELETE":
        try:
            count, _ = Step.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Step.DoesNotExist:
            return JsonResponse(
                {"message": "Step be deleted"},
            )
    else:
        content = json.loads(request.body)
        try:
            Step.objects.filter(id=pk).update(**content)
            step = Step.objects.get(id=pk)
            return JsonResponse(
                {"step": step},
                encoder=StepDetailEncoder,
                safe=False
            )
        except Step.DoesNotExist:
            return JsonResponse(
                {"message": "Step be updated"},
                status=400,
            )


@require_http_methods(['GET', 'POST'])
def api_categories(request):
    if request.method == "GET":
        categories = Category.objects.all()
        return JsonResponse(
            {'categories': categories},
            encoder=CategoryListEncoder
        )
    else:
        content = json.loads(request.body)
        category = Category.objects.create(**content)
        return JsonResponse(
            category,
            encoder=CategoryListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "PUT"])
def api_category(request, pk):
    if request.method == "DELETE":
        try:
            count, _ = Category.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Category.DoesNotExist:
            return JsonResponse(
                {"message": "Category cannot be deleted"},
            )
    else:
        content = json.loads(request.body)
        try:
            Category.objects.filter(id=pk).update(**content)
            category = Category.objects.get(id=pk)
            return JsonResponse(
                {"category": category},
                encoder=CategoryListEncoder,
                safe=False
            )
        except Category.DoesNotExist:
            return JsonResponse(
                {"message": "Category cannot be updated"},
                status=400,
            )


@require_http_methods(["GET"])
def api_drinks_by_category(request, cat_id):
    if request.method == "GET":
        drinks = Drink.objects.filter(category=cat_id)
        return JsonResponse(
            {"drinks": drinks},
            encoder=DrinkEncoder,
        )


@require_http_methods(["GET"])
def api_drinks_by_user(request, user_id):
    if request.method == "GET":
        drinks = Drink.objects.filter(creator=user_id)
        return JsonResponse(
            {"drinks": drinks},
            encoder=DrinkEncoder,
        )
