from django.test import TestCase, Client

from accounts.models import UserAccount
from .models import Drink, Category, Review, IngredientItem, Step

client = Client()


# Menger's test
class CategoryTestCase(TestCase):
    def setUp(self):
        Category.objects.create(name="sweet")

    def test_category_create(self):
        category = Category.objects.get(name="sweet")
        self.assertEqual(category.name, "sweet")


# Malik's test
class TestDrinkModel(TestCase):
    def setUp(self):
        UserAccount.objects.create(
            id=1,
            email="test@test.com",
            first_name='test',
            last_name='test',
            birthday='1950-01-01',
        )
        Category.objects.create(id=1, name="Classic")

        Drink.objects.create(
            id=1,
            name="Cosmopolitan",
            alcoholic="True",
            description="Refreshing",
            creator_id=1,
            category_id=1
        )

    def test_api_get_drinks_200(self):
        response = client.get("http://localhost:8000/api/drinks/")
        self.assertEqual(response.status_code, 200, "Hello")


# Jonathan Chen
class TestReviewModel(TestCase):
    def setUp(self):
        UserAccount.objects.create(
            id=1,
            email="joe@joemama.com",
            first_name='joe',
            last_name='mama',
            birthday='1969-06-09',
        )

        Category.objects.create(id=1, name="old-fashion")

        Drink.objects.create(
            id=1,
            name="Moscow Mule",
            alcoholic="True",
            description="Hard-Hitting",
            creator_id=1,
            category_id=1
        )

        Review.objects.create(
            rating=1,
            creator_id=1,
            drink_id=1,
            comment="WOW!"
        )

    def test_create_review(self):
        response = client.get("http://localhost:8000/api/reviews/1/")
        self.assertEqual(response.status_code, 200, "SUCCESS")


# John Eger
class TestStepModel(TestCase):
    def setUp(self):
        UserAccount.objects.create(
            id=1,
            email="test1@test.com",
            first_name="john",
            last_name="doe",
            birthday="1989-10-10",
        )
        Category.objects.create(id=1, name="Step")

        Drink.objects.create(
            id=1,
            name="Vodka Redbull",
            alcoholic="True",
            description="Late night pick up",
            creator_id=1,
            category_id=1,
        )

        Step.objects.create(
            drink_id=1,
            order="1",
            directions="Add all ingredients together",
        )

    def test_create_step(self):
        response = client.get("http://localhost:8000/api/step/1")
        self.assertEqual(response.status_code, 200, "Great success!")


# Jason
class TestIngredientModel(TestCase):
    def setUp(self):
        UserAccount.objects.create(
            id=1,
            email="test@test.com",
            first_name="test",
            last_name="test last",
            birthday="1990-01-01"
        )

        Category.objects.create(id=1, name="test")

        Drink.objects.create(
            id=1,
            name="Test",
            alcoholic="True",
            description="Not for kids",
            creator_id=1,
            category_id=1
        )

        IngredientItem.objects.create(
            name="vodka",
            amount="2",
            measurement="shots",
            drink_id=1
        )

    def test_create_ingredient(self):
        response = client.get("http://localhost:8000/api/ingredient/1/")
        self.assertEqual(response.status_code, 200, "SUCCESS")
