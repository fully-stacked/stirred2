from .common.json import ModelEncoder
from accounts.models import UserAccount
from .models import (
    Drink,
    Step,
    Review,
    IngredientItem,
    Category
)


class CategoryListEncoder(ModelEncoder):
    model = Category
    properties = ['id', 'name']


class UserListEncoder(ModelEncoder):
    model = UserAccount
    properties = ['id', 'first_name']


class DrinkEncoder(ModelEncoder):
    model = Drink
    properties = ['id', 'name', 'alcoholic', 'description', 'creator', 'category', 'picture_url']
    encoders = {
        'creator': UserListEncoder(),
        'category': CategoryListEncoder()
    }


class ReviewListEncoder(ModelEncoder):
    model = Review
    properties = ['id', 'rating', 'creator', 'comment']
    encoders = {
        'creator': UserListEncoder(),
        'drink': DrinkEncoder(),
    }


class ReviewDetailEncoder(ModelEncoder):
    model = Review
    properties = ['id', 'rating', 'creator', 'drink', 'comment']
    encoders = {
        'creator': UserListEncoder(),
        'drink': DrinkEncoder(),
    }


class IngredientListEncoder(ModelEncoder):
    model = IngredientItem
    properties = ['id', 'name', 'amount', 'measurement']


class IngredientDetailEncoder(ModelEncoder):
    model = IngredientItem
    properties = ['id', 'name', 'amount', 'measurement', 'drink']
    encoders = {
        'drink': DrinkEncoder()
    }


class StepListEncoder(ModelEncoder):
    model = Step
    properties = ['id', 'order', 'directions']


class StepDetailEncoder(ModelEncoder):
    model = Step
    properties = ['id', 'order', 'directions', 'drink']
    encoders = {
        "drink": DrinkEncoder()
    }
