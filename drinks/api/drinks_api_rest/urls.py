from django.urls import path
from .views import (api_ingredient_item,
                    api_ingredient_items,
                    api_review,
                    api_reviews,
                    api_steps,
                    api_step,
                    api_drinks,
                    api_drink,
                    api_all_reviews,
                    api_categories,
                    api_category,
                    api_drinks_by_category,
                    api_users,
                    api_drinks_by_user,)


urlpatterns = [

    path("drink/<int:drink_id>/ingredients/", api_ingredient_items,
         name="api_ingredient_items"),
    path("drink/ingredient/<int:pk>/",
         api_ingredient_item, name="api_ingredient_item"),

    path("reviews/<int:drink_id>/", api_reviews, name="api_reviews"),
    path("review/<int:pk>/", api_review, name="api_review"),

    path("drinks/<int:drink_id>/steps/", api_steps, name="api_steps"),
    path("drink/step/<int:pk>/", api_step, name="api_step"),

    path("drinks/", api_drinks, name="api_drinks"),
    path("drinks/<int:drink_id>/", api_drink, name="api_drink"),

    path("categories/", api_categories, name="api_categories"),
    path("category/<int:pk>/", api_category, name="api_category"),
    path("drinks/category/<int:cat_id>/", api_drinks_by_category, name="api_drinks_by_category"),
    path("users/", api_users, name="api_users"),
    path("user/drinks/<int:user_id>/", api_drinks_by_user, name="api_drinks_by_user"),

    path("reviews/", api_all_reviews, name="reviews"), # Delete later

]

